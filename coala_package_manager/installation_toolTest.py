#!/usr/bin/env python

import os
import unittest
from unittest.mock import patch
from dependency_management.requirements.PipRequirement import (
    PipRequirement)
from coala_utils.ContextManagers import retrieve_stdout
from installation_tool import *


class GetOutputTest(unittest.TestCase):

    def test_echo_command(self):
        self.test_command = ['echo', '"Hello"']
        self.output = get_output(self.test_command)
        self.assertEqual('"Hello"\n', self.output)


class GetAllBearsTest(unittest.TestCase):

    def setUp(self):
        print("\nGetAllBearsTest: Setting Up Class")
        self.all_bears = get_all_bears_names_from_PyPI()

    def test_individual_bears(self):
        # Is there a better way to check of all the bears are there
        # instead of checking random bears? Like store them manually
        # in a text file or something? That is the only way to test
        # this method properly I guess!
        self.assertIn('PyLintBear', self.all_bears)
        self.assertIn('PEP8Bear', self.all_bears)


class InstallRequirementsTest(unittest.TestCase):
    """
    REQUIRES UPDATE OF ALL BEARS
    1.  How should I check the case where the bear is installed but fails
        in installing it's requirements? ('FAILED!'' case)? (line 83) -
        QuotesBear
    2.  Also, how should I test OSError for 100% coverage? (line 87) - ?
    """
    @classmethod
    def setUpClass(cls):
        print("\nInstallRequirementsTest: Setting Up Class")
        install_bears(['PEP8Bear'], False)
        # Consider a bear whose dependencies are not installed
        # How do we do that?
        install_bears(['ScalaLintBear'], True)

    @classmethod
    def tearDownClass(cls):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('ScalaLintBear').uninstall_package()

    def test_all_requirements_installed(self):
        print("\nInstallRequirementsTest:test_all_requirements_installed")
        with retrieve_stdout() as stdout:
            self.assertEqual([], install_requirements('PEP8Bear'))

    """ Needs discussion.
    def test_not_all_requirements_installed(self):
        print("\nInstallRequirementsTest:test_not_all_requirements_installed")
        with retrieve_stdout() as stdout:
            self.assertEqual([], install_requirements('ScalaLintBear'))
            self.assertIn('DONE!', stdout.getvalue())
    """

    def test_not_installed_bear(self):
        print("\nInstallRequirementsTest:test_not_installed_bear")
        self.err_msg = ("No module named 'coalaPyLintBear'")
        try:
            install_requirements('PyLintBear')
        except ImportError as err:
            self.assertIn(self.err_msg, str(err))


class CheckRequirementsTest(unittest.TestCase):
    """
    REQUIRES UPDATE OF ALL BEARS
    1.  For testing missing dependency condition, I have used QuotesBear
        and ignore_deps as True. Is there any other bear which has requirements
        ? This doesn't work!
        for sure? I'll use that! (to get 100% coverage) - QuotesBear?
    """

    def test_requirements_installed(self):
        install_bears(['PEP8Bear'], False)
        print("\nCheckRequirementsTest:test_requirements_installed")
        with retrieve_stdout() as stdout:
            check_requirements('PEP8Bear')
            self.assertIn('autopep8 is installed.\n', stdout.getvalue())
        PipRequirement('PEP8Bear').uninstall_package()

    def test_bear_not_installed(self):
        install_bears(['PEP8Bear'], False)
        # Removing one of the requirements of PEP8Bear for testing.
        PipRequirement('autopep8').uninstall_package()
        print("\nCheckRequirementsTest:test_bear_not_installed")
        with retrieve_stdout() as stdout:
            check_requirements('PEP8Bear')
            self.assertIn('autopep8 is not installed.\n', stdout.getvalue())
        PipRequirement('PEP8Bear').uninstall_package()

    def test_requirements_not_installed(self):
        print("\nCheckRequirementsTest:test_requirements_not_installed")
        with retrieve_stdout() as stdout:
            self.assertEqual(1, check_requirements('PyLintBear'))
            self.assertIn('PyLintBear has missing dependencies.\n',
                          stdout.getvalue())


class InstallBearsTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        print("\nInstallBearsTest: Setting Up Class")
        install_bears(['PEP8Bear'], True)

    def test_installed_bear(self):
        with retrieve_stdout() as stdout:
            self.assertEqual([], install_bears(['PEP8Bear'], True))
            self.assertIn('PEP8Bear is already installed.', stdout.getvalue())

    def test_install_bear(self):
        with retrieve_stdout() as stdout:
            self.assertEqual([], install_bears(['PyLintBear'], True))
            self.assertIn('DONE!', stdout.getvalue())

    def test_fail_install_bear(self):
        with retrieve_stdout() as stdout:
            self.assertEqual(['TestBear'], install_bears(['TestBear'], True))
            self.assertIn('FAILED!', stdout.getvalue())

    @classmethod
    def tearDownClass(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('PyLintBear').uninstall_package()


class ShowInstalledBearsTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        print("\nShowInstalledBearsTest: Setting Up Class")
        self.installed_bears = ['PEP8Bear', 'PyLintBear']
        self.not_all_installed_bears = ['PEP8Bear', 'PyLintBear', 'TestBear']
        install_bears(self.installed_bears, True)

    def test_all_bears_installed(self):
        self.assertTrue(show_installed_bears(self.installed_bears))

    def test_not_all_bears_installed(self):
        self.assertFalse(show_installed_bears(self.not_all_installed_bears))

    @classmethod
    def tearDownClass(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('PyLintBear').uninstall_package()


class MainTest(unittest.TestCase):

    def setUp(self):
        print("\n\tMainTest: Setting Up Method")
        self.all_bears = get_all_bears_names_from_PyPI()
        self.argv = ['installation_tool.py']
        argv_patcher = patch.object(sys, 'argv', self.argv)
        self.addCleanup(argv_patcher.stop)
        self.argv_mock = argv_patcher.start()

    def tearDown(self):
        print("\n\tMainTest: Ending Method")

        # Uninstall any installed bear
        for each_bear in self.all_bears:
            if PipRequirement(each_bear).is_installed():
                PipRequirement(each_bear).uninstall_package()

        # Uninstall coala-bears if installed
        if PipRequirement('coala-bears').is_installed():
            PipRequirement('coala-bears').uninstall_package()

    def test_list_bears(self):
        """
        Tests stdout output for 'cib list' command
        """
        print("\nMainTest:test_list_bears")
        self.beg_line = 'This is a list of all the bears you can install:\n'
        self.argv.append('list')
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
            for self.bear in self.all_bears:
                self.assertIn(self.bear, stdout.getvalue())

    """
    def test_install_all_bears(self):
        Tests stdout output for 'cib install all' command

        ### But, this installs all bears ignoring their dependencies. Is
        ### the right thing to do?

        ### Is there a better way to test this?

        print("\nMainTest:test_install_all_bears")
        self.beg_line = 'Great idea, we are installing all the bears right now'
        self.argv += ['install', 'all']
        with retrieve_stdout() as stdout:
            main()
            for each_bear in self.all_bears:
                self.assertTrue(PipRequirement(each_bear).is_installed())

            self.assertIn(self.beg_line, stdout.getvalue())
    """

    def test_coala_bears_installed(self):
        """
        Tests error message thrown when installing any bear
        when coala-bears is already installed.
        """
        print("\nMainTest:test_coala_bears_installed")
        PipRequirement('coala-bears').install_package()
        self.argv += ['install', 'all']
        with self.assertRaises(SystemExit) as cm:
            main()
            self.assertEqual(cm.exception.code, 1)

    def test_install_bears_without_dependency(self):
        """
        Tests installation of bears without their dependencies.
        Tests stdout output for 'cib --ignore-deps install PEP8Bear
        PylintBear' command.
        """
        print("\nMainTest:test_install_bears_without_dependency")
        self.argv += ['--ignore-deps', 'install', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('PEP8Bear is installing', stdout.getvalue())
            self.assertIn('PyLintBear is installing', stdout.getvalue())
            self.assertIn('DONE!', stdout.getvalue())

    def test_install_invalid_bears(self):
        """
        Tests installation of bears which are invalid.
        Tests stdout output for 'cib install TestBear' command.
        """
        print("\nMainTest:test_install_invalid_bears")
        self.err_msg = '\nThe following inputs were not part of the bears list'
        self.argv += ['install', 'TestBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())
            self.assertIn('testbear', stdout.getvalue())

    def test_given_coafile_installation(self):
        """
        Tests installation through given coafile.
        Tests stdout output for 'cib install -c my_coafile' command
        Needs a better coafile? Help here!!
        """
        print("\nMainTest:test_given_coafile_installation")
        self.argv += ['install', '-c', 'my_coafile']
        self.sample_coafile_string = ('[default]\nenabled = True\noverridable'
                                      ' = 2\n\n[section-1]\nenabled = True\n'
                                      'overridable = 3\nother = 4 ')
        self.coafile = open('my_coafile', 'w')
        self.coafile.write(self.sample_coafile_string)
        self.coafile.close()
        self.beg_line = 'Checking my_coafile for installation\n'
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
        os.remove('my_coafile')

    def test_default_coafile_installation(self):
        """
        Tests installation through default coafile.
        Tests stdout output for 'cib install -c' command
        Needs a better default coafile? Help here!!
        """
        print("\nMainTest:test_default_coafile_installation")
        self.argv += ['install', '-c']
        self.sample_coafile_string = ('[default]\nenabled = True\noverridable'
                                      ' = 2\n\n[section-1]\nenabled = True\n'
                                      'overridable = 3\nother = 4 ')
        self.coafile = open('.coafile', 'w')
        self.coafile.write(self.sample_coafile_string)
        self.coafile.close()
        self.beg_line = 'Checking .coafile for installation\n'
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
        os.remove('.coafile')

    def test_upgrade_invalid_bears(self):
        """
        Tests upgradation of bears which are invalid.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib upgrade TestBear' command
        """
        print("\nMainTest:test_upgrade_invalid_bears")
        self.err_msg = ('\nThe following inputs were not bears or were not '
                        'installed and were therefore not upgraded:\n'
                        'testbear\n')
        self.argv += ['upgrade', 'TestBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertEqual(self.err_msg, stdout.getvalue())

    def test_upgrade_not_installed_bears(self):
        """
        Tests upgradation of bears which are not installed.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib upgrade ScalaLintBear' command
        """
        print("\nMainTest:test_upgrade_not_installed_bears")
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not upgraded:\n')
        self.argv += ['upgrade', 'ScalaLintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_upgrade_installed_bears(self):
        """
        Tests upragadation of certain installed bears.
        Tests stdout output for 'cib upgrade PEP8Bear PyLintBear' command
        """
        print("\nMainTest:test_upgrade_installed_bears")
        install_bears(['PEP8Bear', 'PylintBear'], True)
        self.argv += ['upgrade', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Upgrading PEP8Bear now..', stdout.getvalue())
            self.assertIn('Upgrading PyLintBear now..', stdout.getvalue())

    """
    def test_upgrade_all_bears(self):
        #
        #Tests upgradation of all bears.
        #Tests stdout output for 'cib upgrade all' command
        #
        print("\nMainTest:test_upgrade_all_bears")
        self.beg_line = ('Great idea, we are upgrading all the installed '
                         'bears right now.')
        install_bears(self.all_bears, True)
        self.argv += ['upgrade', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
            for each_bear in self.all_bears:
                self.assertIn('Upgrading ' + each_bear +
                              ' now..', stdout.getvalue())
    """

    def test_uninstall_invalid_bears(self):
        """
        Tests uninstallation of bears which are invalid.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib install TestBear' command
        """
        print("\nMainTest:test_uninstall_invalid_bears")
        self.err_msg = ('\nThe following inputs were not bears and were '
                        'therefore not uninstalled:\ntestbear\n')
        self.argv += ['uninstall', 'TestBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertEqual(self.err_msg, stdout.getvalue())

    def test_uninstall_not_installed_bears(self):
        """
        Tests uninstallation of bears which are not installed.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib uninstall ScalaLintBear' command
        """
        print("\nMainTest:test_uninstall_not_installed_bears")
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not uninstalled:\n')
        self.argv += ['uninstall', 'ScalaLintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_uninstall_installed_bears(self):
        """
        Tests uninstallation of certain installed bears.
        Tests stdout output for 'cib uninstall PEP8Bear PyLintBear' command
        """
        print("\nMainTest:test_uninstall_installed_bears")
        install_bears(['PEP8Bear', 'PyLintBear'], True)
        self.argv += ['uninstall', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Uninstalling PEP8Bear now..', stdout.getvalue())
            self.assertIn('Uninstalling PyLintBear now..', stdout.getvalue())

    """
    def test_uninstall_all_bears(self):
        #
        #Tests uninstallation of all bears.
        #Tests stdout output for 'cib uninstall all' command
        #
        print("\nMainTest:test_uninstall_all_bears")
        install_bears(self.all_bears, True)
        self.beg_line = 'Bad idea, we are uninstalling all the installed bears'
        self.argv += ['uninstall', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
            for each_bear in self.all_bears:
                self.assertIn('Uninstalling ' + each_bear +
                              ' now..', stdout.getvalue())
    """

    def test_checkdep_invalid_bears(self):
        """
        Tests Checking dependencies of bears which are invalid.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib check-deps TestBear' command
        """
        print("\nMainTest:test_checkdep_invalid_bears")
        self.err_msg = ('\nThe following inputs were not bears and were '
                        'therefore not checked:\ntestbear\n')
        self.argv += ['check-deps', 'TestBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertEqual(self.err_msg, stdout.getvalue())

    def test_checkdep_not_installed_bears(self):
        """
        Tests Checking dependencies of bears which are not installed.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib check-deps ScalaLintBear' command
        """
        print("\nMainTest:test_checkdep_not_installed_bears")
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not checked:\n')
        self.argv += ['check-deps', 'ScalaLintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_checkdep_installed_bears(self):
        """
        Tests Checking dependencies of certain installed bears.
        Tests stdout output for 'cib check-deps PEP8Bear PyLintBear' command
        """
        print("\nMainTest:test_checkdep_installed_bears")
        install_bears(['PEP8Bear', 'PyLintBear'], True)
        self.argv += ['check-deps', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Checking PEP8Bear now..', stdout.getvalue())
            self.assertIn('Checking PyLintBear now..', stdout.getvalue())

    """
    def test_checkdep_all_bears(self):
        #
        #Tests Checking dependencies of all bears.
        #Tests stdout output for 'cib check-deps all' command
        #
        print("\nMainTest:test_checkdep_all_bears")
        install_bears(self.all_bears, True)
        self.beg_line = 'Good idea, we are checking all the installed '
        self.argv += ['check-deps', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.beg_line, stdout.getvalue())
            for each_bear in self.all_bears:
                self.assertIn('Checking ' + each_bear +
                              ' now..', stdout.getvalue())
    """

    def test_show_invalid_bear(self):
        """
        Tests show command for bears which are invalid.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib show TestBear' command
        """
        self.argv += ['show', 'TestBear']
        self.err_msg = ('\nThe following bear is invalid and cannot be shown. '
                        'Please see `cib list` to see the full list of '
                        'bears.\n')
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())
            self.assertIn('testbear', stdout.getvalue())

    def test_show_not_installed_bear(self):
        """
        Tests show command for bears which are not installed.
        The appropiate error message thrown is checked here.
        Tests stdout output for 'cib show HaskellLintBear' command
        """
        self.argv += ['show', 'HaskellLintBear']
        self.err_msg = ('\nThe following bear is not installed. Please use '
                        '`cib install HaskellLintBear` to install it.\n\n')
        with retrieve_stdout() as stdout:
            main()
            self.assertEqual(self.err_msg, stdout.getvalue())

    def test_show_installed_bear(self):
        """
        Tests installation of certain already installed bears.
        Tests stdout output for 'cib show PEP8Bear' command
        """
        install_bears(['PEP8Bear'], False)
        self.argv += ['show', 'PEP8Bear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Detects and fixes PEP8 incompliant code. This bear'
                          ' will not change functionality of the code in any'
                          ' way.\n', stdout.getvalue())


if __name__ == '__main__':
    unittest.main()
